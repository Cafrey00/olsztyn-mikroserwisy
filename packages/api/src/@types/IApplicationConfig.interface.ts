export interface IApplicationConfig {
    NODE_PORT: number;
    API: {
        AUTH_URL: string;
        TODOS_URL: string;
    }
    JWT: {
        SECRET: string;
        EXPIRES_IN_SECONDS: number;
    }
}
