import {SetMetadata} from '@nestjs/common';

export const AccessPublic = () => SetMetadata('isPublic', true);
