import {ExtractJwt, Strategy} from 'passport-jwt';
import {PassportStrategy} from '@nestjs/passport';
import {Injectable, UnauthorizedException} from '@nestjs/common';
import {env} from "../env";
import {AuthService} from "../auth/auth.service";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(private authService: AuthService) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: env.JWT.SECRET,
        });
    }

    async validate(payload: any) {
        try {
            return await this.authService.getUserById(payload.sub).toPromise();
        } catch (err) {
            console.log(err);
            throw new UnauthorizedException();
        }
    }
}
