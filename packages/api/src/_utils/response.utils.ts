export const handleSuccessResponse = (res, statusCode: Number) => (response: any) => res.status(statusCode).send(response);
export const handleErrorResponse = res => error => res.status(error.response.data.statusCode).send(error.response.data)
