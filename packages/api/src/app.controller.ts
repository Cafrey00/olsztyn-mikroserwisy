import {Body, Controller, Delete, Get, Param, ParseUUIDPipe, Post, Put, Req, Res} from '@nestjs/common';
import {TodoService} from "./todo/todo.service";
import {AuthService} from "./auth/auth.service";
import {handleErrorResponse, handleSuccessResponse} from "./_utils/response.utils";
import {RegisterUserDto} from "./@types/dto/registerUser.dto";
import {LoginUserDto} from "./@types/dto/loginUser.dto";
import {AccessPublic} from "./_decarators/public.decorator";
import {TodoDto} from "./@types/dto/todo.dto";

@Controller()
export class AppController {
    constructor(private readonly todoService: TodoService,
                private readonly authService: AuthService) {
    }

    @AccessPublic()
    @Post('auth/register')
    public register(@Res() res,
                    @Body() body: RegisterUserDto) {
        this.authService.register(body).subscribe(
            handleSuccessResponse(res, 200),
            handleErrorResponse(res)
        );
    }

    @AccessPublic()
    @Post('auth/login')
    public login(@Res() res,
                 @Body() body: LoginUserDto) {
        this.authService.login(body).subscribe(
            handleSuccessResponse(res, 200),
            handleErrorResponse(res)
        );
    }

    @Get('auth/userLogins')
    public getUserLogins(@Req() req,
                         @Res() res) {
        this.authService.getUserLogins(req.user.id).subscribe(
            handleSuccessResponse(res, 200),
            handleErrorResponse(res)
        );
    }

    @Get('todos')
    public getTodoList(@Req() req,
                       @Res() res) {
        this.todoService.getTodoList(req.user.id).subscribe(
            handleSuccessResponse(res, 200),
            handleErrorResponse(res)
        )
    }

    @Get('todos/:todoId')
    public getTodoItem(@Req() req,
                       @Res() res,
                       @Param('todoId', new ParseUUIDPipe()) todoId: string) {
        this.todoService.getTodoItem(req.user.id, todoId).subscribe(
            handleSuccessResponse(res, 200),
            handleErrorResponse(res)
        )
    }

    @Post('todos')
    public addTodoItem(@Req() req,
                       @Res() res,
                       @Body() todoDto: TodoDto) {
        this.todoService.addTodoItem(req.user.id, todoDto).subscribe(
            handleSuccessResponse(res, 200),
            handleErrorResponse(res)
        )
    }

    @Put('todos/:todoId')
    public editTodoItem(@Req() req,
                        @Res() res,
                        @Param('todoId', new ParseUUIDPipe()) todoId: string,
                        @Body() todoDto: TodoDto) {
        this.todoService.editTodoItem(req.user.id, todoId, todoDto).subscribe(
            handleSuccessResponse(res, 200),
            handleErrorResponse(res)
        )
    }

    @Delete('todos/:todoId')
    public deleteTodoItem(@Req() req,
                          @Res() res,
                          @Param('todoId', new ParseUUIDPipe()) todoId: string) {
        this.todoService.deleteTodoItem(req.user.id, todoId).subscribe(
            handleSuccessResponse(res, 200),
            handleErrorResponse(res)
        )
    }

}
