import {HttpModule, Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {TodoService} from './todo/todo.service';
import {AuthService} from './auth/auth.service';
import {JwtGuard} from "./_guards/jwt.guard";
import {APP_GUARD} from "@nestjs/core";
import {JwtStrategy} from "./_strategies/jwt.strategy";
import {JwtModule} from "@nestjs/jwt";
import {env} from "./env";

@Module({
    imports: [
        HttpModule,
        JwtModule.register({
            secret: env.JWT.SECRET,
            signOptions: {expiresIn: env.JWT.EXPIRES_IN_SECONDS + 's'},
        }),
    ],
    controllers: [AppController],
    providers: [{
        provide: APP_GUARD,
        useClass: JwtGuard,
    }, JwtStrategy, AppService, TodoService, AuthService],
})
export class AppModule {
}
