import {ILoginResponse} from "./login.response";

export interface IAuthResponse {
    user: ILoginResponse;
    token: string;
}
