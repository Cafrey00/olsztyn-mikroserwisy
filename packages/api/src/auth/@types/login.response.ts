export interface ILoginResponse {
    id: string;
    firstName: string;
    lastName: string;
    email: string;
}
