export interface IUserLoginsResponse {
    id: string;
    createdAt: string;
}
