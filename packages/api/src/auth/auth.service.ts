import {HttpService, Injectable} from '@nestjs/common';
import {env} from "../env";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {AxiosResponse} from "axios";
import {ILoginResponse} from "./@types/login.response";
import {RegisterUserDto} from "../@types/dto/registerUser.dto";
import {LoginUserDto} from "../@types/dto/loginUser.dto";
import {IAuthResponse} from "./@types/auth.response";
import {JwtService} from "@nestjs/jwt";
import {IUserLoginsResponse} from "./@types/userLogins.response";

@Injectable()
export class AuthService {
    constructor(private readonly httpService: HttpService,
                private readonly jwtService: JwtService) {
    }

    public register(body: RegisterUserDto): Observable<IAuthResponse> {
        return this.httpService.post<ILoginResponse>(`${env.API.AUTH_URL}/auth/register`, body)
            .pipe(map((response: AxiosResponse<ILoginResponse>) => this.mapLoginResponseToUserWithToken(response.data)));
    }

    public login(body: LoginUserDto): Observable<IAuthResponse> {
        return this.httpService.post<ILoginResponse>(`${env.API.AUTH_URL}/auth/login`, body)
            .pipe(map((response: AxiosResponse<ILoginResponse>) => this.mapLoginResponseToUserWithToken(response.data)));
    }

    public getUserById(userId: string): Observable<ILoginResponse> {
        return this.httpService.get<ILoginResponse>(`${env.API.AUTH_URL}/auth/getById/${userId}`)
            .pipe(map((response: AxiosResponse<ILoginResponse>) => response.data));
    }

    public getUserLogins(userId: string): Observable<IUserLoginsResponse[]> {
        return this.httpService.get<IUserLoginsResponse[]>(`${env.API.AUTH_URL}/auth/userLogins/${userId}`)
            .pipe(map((response: AxiosResponse<IUserLoginsResponse[]>) => response.data));
    }

    private mapLoginResponseToUserWithToken(loginResponse: ILoginResponse): IAuthResponse {
        const payload = {
            sub: loginResponse.id,
        }
        const token = this.jwtService.sign(payload);
        return {
            user: loginResponse,
            token
        }
    }

}
