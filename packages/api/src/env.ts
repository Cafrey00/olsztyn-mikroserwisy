import {IApplicationConfig} from "./@types/IApplicationConfig.interface";

require('dotenv').config();

export const env: IApplicationConfig = {
    NODE_PORT: parseInt(process.env.NODE_PORT),
    API: {
        AUTH_URL: process.env.API_AUTH_URL,
        TODOS_URL: process.env.API_TODOS_URL,
    },
    JWT: {
        SECRET: process.env.JWT_SECRET || 'jwtSecret',
        EXPIRES_IN_SECONDS: parseInt(process.env.JWT_EXPIRES_IN_SECONDS) || 3600
    }
}
