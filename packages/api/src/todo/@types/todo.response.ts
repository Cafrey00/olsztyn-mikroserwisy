export interface ITodoResponse {
    id: string;
    title: string;
    description: string;
    finished: string;
    createdAt: string;
    updatedAt: string;
    deletedAt: string;
}
