import {ITodoResponse} from "./todo.response";

export interface ITodoListResponse {
    count: number;
    entries: ITodoResponse[];
}
