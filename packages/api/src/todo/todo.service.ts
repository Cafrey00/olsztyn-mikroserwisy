import {HttpService, Injectable} from '@nestjs/common';
import {Observable} from "rxjs";
import {ITodoListResponse} from "./@types/todoList.response";
import {ITodoResponse} from "./@types/todo.response";
import {env} from "../env";
import {map} from "rxjs/operators";
import {AxiosResponse} from "axios";
import {TodoDto} from "../@types/dto/todo.dto";

@Injectable()
export class TodoService {
    constructor(private readonly httpService: HttpService) {
    }

    getTodoList(userId: string): Observable<ITodoListResponse> {
        return this.httpService.get<ITodoListResponse>(`${env.API.TODOS_URL}/todo/${userId}`)
            .pipe(map((response: AxiosResponse<ITodoListResponse>) => {
                console.log(response.data);
                return response.data
            }));
    }

    getTodoItem(userId: string, todoId: string): Observable<ITodoResponse> {
        return this.httpService.get<ITodoResponse>(`${env.API.TODOS_URL}/todo/${userId}/${todoId}`)
            .pipe(map((response: AxiosResponse<ITodoResponse>) => response.data));
    }

    addTodoItem(userId: string, todoDto: TodoDto): Observable<ITodoResponse> {
        return this.httpService.post<ITodoResponse>(`${env.API.TODOS_URL}/todo/${userId}`, todoDto)
            .pipe(map((response: AxiosResponse<ITodoResponse>) => response.data));

    }

    editTodoItem(userId: string, todoId: string, todoDto: TodoDto): Observable<ITodoResponse> {
        return this.httpService.put<ITodoResponse>(`${env.API.TODOS_URL}/todo/${userId}/${todoId}`, todoDto)
            .pipe(map((response: AxiosResponse<ITodoResponse>) => response.data));
    }

    deleteTodoItem(userId: string, todoId: string): Observable<void> {
        return this.httpService.delete<void>(`${env.API.TODOS_URL}/todo/${userId}/${todoId}`)
            .pipe(map((response: AxiosResponse<void>) => response.data));
    }


}
