import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    CreateDateColumn,
    UpdateDateColumn,
    DeleteDateColumn,
    OneToMany
} from 'typeorm';
import {UserLogin} from "./UserLogin.entity";

@Entity()
export class User {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({length: 255, nullable: false})
    firstName: string;

    @Column({length: 255, nullable: false})
    lastName: string;

    @Column({length: 255, nullable: false, unique: true})
    email: string;

    @Column({length: 255, nullable: false, select: false})
    password: string;

    @Column({length: 255, nullable: false, select: false})
    salt: string;

    @OneToMany(type => UserLogin, userLogin => userLogin.user, {
        onDelete: 'CASCADE'
    })
    userLogins: UserLogin[];

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @DeleteDateColumn()
    deletedAt: Date;
}
