import {CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {User} from "./User.entity";

@Entity()
export class UserLogin {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => User, user => user.userLogins, {
        onDelete: 'NO ACTION',
    })
    user: User;

    @CreateDateColumn()
    createdAt: Date;
}
