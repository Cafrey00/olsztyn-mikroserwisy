import {User} from "../_entity/User.entity";
import {EntityRepository, Repository} from "typeorm";
import {RegisterUserDto} from "../auth/@types/dto/registerUser.dto";

@EntityRepository(User)
export class UserRepository extends Repository<User> {

    public async getUserWithCredentialsByEmail(email: string): Promise<User> {
        return this.createQueryBuilder('user')
            .addSelect(['user.password', 'user.salt'])
            .leftJoinAndSelect('user.userLogins', 'userLogins')
            .where('user.email = :email', {email: email.toLowerCase().trim()})
            .getOne();
    }

    public async addUser(registerUserDto: RegisterUserDto, salt: string, password: string): Promise<User> {
        return this.save({
            firstName: registerUserDto.firstName,
            lastName: registerUserDto.lastName,
            email: registerUserDto.email.toLowerCase().trim(),
            password,
            salt,
        });
    }

}
