import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {env} from './env';
import {User} from "./_entity/User.entity";
import {AuthController} from './auth/auth.controller';
import {UsersController} from './users/users.controller';
import {AuthService} from './auth/auth.service';
import {UsersService} from './users/users.service';
import {UserRepository} from "./_repository/User.repository";
import {UserLogin} from "./_entity/UserLogin.entity";

@Module({
    imports: [
        TypeOrmModule.forRoot({
            type: 'mysql',
            host: env.DB.HOST,
            port: env.DB.PORT,
            username: env.DB.USERNAME,
            password: env.DB.PASSWORD,
            database: env.DB.NAME,
            entities: [User, UserLogin],
            synchronize: true,
        }),
        TypeOrmModule.forFeature([UserRepository, UserLogin])
    ],
    controllers: [AppController, AuthController, UsersController],
    providers: [AppService, AuthService, UsersService],
})
export class AppModule {
}
