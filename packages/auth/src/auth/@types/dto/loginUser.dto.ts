import {IsEmail, IsString, Length} from "class-validator";

export class LoginUserDto {
    @IsEmail()
    @Length(3, 255)
    public readonly email: string;

    @IsString()
    @Length(6, 30)
    public readonly password: string;
}
