export interface IUserAuthResponse {
    id: string;
    firstName: string;
    lastName: string;
    email: string;
}
