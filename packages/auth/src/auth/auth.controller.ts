import {Body, Controller, Get, Param, ParseUUIDPipe, Post, Req} from '@nestjs/common';
import {RegisterUserDto} from "./@types/dto/registerUser.dto";
import {AuthService} from "./auth.service";
import {LoginUserDto} from "./@types/dto/loginUser.dto";
import {UserLogin} from "../_entity/UserLogin.entity";
import {IUserAuthResponse} from "./@types/userAuth.response";

@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService) {
    }

    @Post('register')
    public registerUser(@Body() registerUserDto: RegisterUserDto): Promise<IUserAuthResponse> {
        return this.authService.registerUser(registerUserDto);
    }

    @Post('login')
    public loginUser(@Body() loginUserDto: LoginUserDto): Promise<IUserAuthResponse> {
        return this.authService.loginUser(loginUserDto);
    }

    @Get('getById/:userId')
    public getUserById(@Param('userId', new ParseUUIDPipe()) userId: string): Promise<IUserAuthResponse> {
        return this.authService.findUserById(userId);
    }

    @Get('userLogins/:userId')
    public getUserLogins(@Param('userId', new ParseUUIDPipe()) userId: string): Promise<UserLogin[]> {
        return this.authService.getUserLogins(userId)
    }

}
