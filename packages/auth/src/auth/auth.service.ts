import {ConflictException, Injectable, UnauthorizedException} from '@nestjs/common';
import {RegisterUserDto} from "./@types/dto/registerUser.dto";
import {User} from "../_entity/User.entity";
import {UserRepository} from "../_repository/User.repository";
import {hash, genSalt, compare} from 'bcrypt';
import {LoginUserDto} from "./@types/dto/loginUser.dto";
import {InjectRepository} from "@nestjs/typeorm";
import {UserLogin} from "../_entity/UserLogin.entity";
import {Repository} from "typeorm";
import {IUserAuthResponse} from "./@types/userAuth.response";

@Injectable()
export class AuthService {

    constructor(private readonly usersRepository: UserRepository,
                @InjectRepository(UserLogin)
                private readonly userLoginRepository: Repository<UserLogin>) {
    }

    public async findUserById(userId: string): Promise<User> {
        return this.usersRepository.findOne(userId);
    }

    public async registerUser(registerUserDto: RegisterUserDto): Promise<IUserAuthResponse> {
        const salt = await genSalt(5);
        const hashedPassword = await hash(registerUserDto.password, salt);

        try {
            const addedUser = await this.usersRepository.addUser(registerUserDto, salt, hashedPassword);
            return this.loginUser({
                email: addedUser.email,
                password: registerUserDto.password
            });
        } catch (e) {
            throw new ConflictException('User exists!');
        }
    }

    public async loginUser(loginUserDto: LoginUserDto): Promise<IUserAuthResponse> {
        const userWithCredentials = await this.usersRepository.getUserWithCredentialsByEmail(loginUserDto.email);
        if (!userWithCredentials || !userWithCredentials.password || !userWithCredentials.salt) {
            throw new UnauthorizedException();
        }
        const passwordValid = await compare(loginUserDto.password, userWithCredentials.password);
        if (!passwordValid) {
            throw new UnauthorizedException();
        }
        await this.userLoginRepository.save({
            user: userWithCredentials,
        })
        return {
            id: userWithCredentials.id,
            firstName: userWithCredentials.firstName,
            lastName: userWithCredentials.lastName,
            email: userWithCredentials.email,
        };
    }

    public async getUserLogins(userId: string): Promise<UserLogin[]> {
        return this.userLoginRepository.find({
            where: {
                user: userId
            }
        });
    }

}
