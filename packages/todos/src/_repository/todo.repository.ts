import {DeleteResult, EntityRepository, Repository} from "typeorm";
import {Todo} from "../_entity/Todo.entity";
import {TodoDto} from "../todo/@types/dto/todo.dto";
import {ITodoListResponse} from "../todo/@types/todoList.response";

@EntityRepository(Todo)
export class TodoRepository extends Repository<Todo> {

    public async getList(userId: string): Promise<ITodoListResponse> {
        const [entries, count] = await this.createQueryBuilder('todo')
            .andWhere('todo.userId = :userId', {userId})
            .getManyAndCount();
        return {entries, count};
    }

    public async getItem(userId: string, todoId: string): Promise<Todo> {
        return this.createQueryBuilder('todo')
            .where('todo.id = :todoId', {todoId})
            .andWhere('todo.userId = :userId', {userId})
            .getOne();
    }

    public async addItem(userId: string, todoDto: TodoDto): Promise<Todo> {
        const {title, description, finished} = todoDto;
        return this.save({
            title,
            description,
            finished,
            userId,
        })
    }

    public async editItem(todo: Todo, todoDto: TodoDto): Promise<Todo> {
        const {title, description, finished} = todoDto;
        return this.save({
            id: todo.id,
            title,
            description,
            finished,
            userId: todo.userId,
        })
    }

    public async deleteItem(todo: Todo): Promise<DeleteResult> {
        return this.softDelete({id: todo.id});
    }

}
