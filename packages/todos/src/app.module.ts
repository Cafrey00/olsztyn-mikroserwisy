import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {env} from "./env";
import {Todo} from "./_entity/Todo.entity";
import {TodoRepository} from "./_repository/todo.repository";
import { TodoController } from './todo/todo.controller';
import { TodoService } from './todo/todo.service';

@Module({
    imports: [
        TypeOrmModule.forRoot({
            type: 'mysql',
            host: env.DB.HOST,
            port: env.DB.PORT,
            username: env.DB.USERNAME,
            password: env.DB.PASSWORD,
            database: env.DB.NAME,
            entities: [Todo],
            synchronize: true,
        }),
        TypeOrmModule.forFeature([TodoRepository])
    ],
    controllers: [AppController, TodoController],
    providers: [AppService, TodoService],
})
export class AppModule {
}
