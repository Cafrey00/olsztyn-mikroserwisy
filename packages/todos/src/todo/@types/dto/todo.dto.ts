import {IsBoolean, IsOptional, IsString, Length} from "class-validator";

export class TodoDto {
    @Length(3, 500)
    @IsString()
    title: string;

    @IsString()
    @IsOptional()
    description: string;

    @IsBoolean()
    finished: boolean;
}
