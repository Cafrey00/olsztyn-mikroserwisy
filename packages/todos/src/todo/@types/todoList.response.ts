import {Todo} from "../../_entity/Todo.entity";

export interface ITodoListResponse {
    count: number;
    entries: Todo[];
}
