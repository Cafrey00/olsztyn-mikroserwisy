import {Body, Controller, Delete, Get, NotImplementedException, Param, ParseUUIDPipe, Post, Put} from '@nestjs/common';
import {TodoService} from "./todo.service";
import {Todo} from "../_entity/Todo.entity";
import {TodoDto} from "./@types/dto/todo.dto";
import {ITodoListResponse} from "./@types/todoList.response";

@Controller('todo/:userId')
export class TodoController {

    constructor(private readonly todoService: TodoService) {
    }

    @Get()
    getTodoList(@Param('userId', new ParseUUIDPipe()) userId: string): Promise<ITodoListResponse> {
        return this.todoService.getTodoList(userId);
    }

    @Get(':todoId')
    getTodoItem(@Param('userId', new ParseUUIDPipe()) userId: string,
                @Param('todoId', new ParseUUIDPipe()) todoId: string): Promise<Todo> {
        return this.todoService.getTodoItem(userId, todoId);
    }

    @Post()
    addTodo(@Param('userId', new ParseUUIDPipe()) userId: string,
            @Body() todoDto: TodoDto): Promise<Todo> {
        return this.todoService.addTodo(userId, todoDto);
    }

    @Put(':todoId')
    editTodo(@Param('userId', new ParseUUIDPipe()) userId: string,
             @Param('todoId', new ParseUUIDPipe()) todoId: string,
             @Body() todoDto: TodoDto): Promise<Todo> {
        return this.todoService.editTodo(userId, todoId, todoDto);
    }

    @Delete(':todoId')
    async deleteTodo(@Param('userId', new ParseUUIDPipe()) userId: string,
                     @Param('todoId', new ParseUUIDPipe()) todoId: string): Promise<void> {
        await this.todoService.deleteTodo(userId, todoId)
    }
}
