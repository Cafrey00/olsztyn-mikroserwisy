import {Injectable, NotFoundException} from '@nestjs/common';
import {Todo} from "../_entity/Todo.entity";
import {DeleteResult} from "typeorm";
import {TodoDto} from "./@types/dto/todo.dto";
import {TodoRepository} from "../_repository/todo.repository";
import {ITodoListResponse} from "./@types/todoList.response";

@Injectable()
export class TodoService {
    constructor(private readonly todoRepository: TodoRepository) {
    }

    public async getTodoList(userId: string): Promise<ITodoListResponse> {
        return this.todoRepository.getList(userId);
    }

    public async getTodoItem(userId: string, todoId: string): Promise<Todo> {
        const todoItem = await this.todoRepository.getItem(userId, todoId);
        if (!todoItem) {
            throw new NotFoundException(`Cannot find todo item with given id (${todoId})`);
        }
        return todoItem;
    }

    public async addTodo(userId: string, todoDto: TodoDto): Promise<Todo> {
        return this.todoRepository.addItem(userId, todoDto);
    }

    public async editTodo(userId: string, todoId: string, todoDto: TodoDto): Promise<Todo> {
        const todoItem = await this.getTodoItem(userId, todoId);
        return this.todoRepository.editItem(todoItem, todoDto);
    }

    public async deleteTodo(userId: string, todoId: string): Promise<DeleteResult> {
        const todoItem = await this.getTodoItem(userId, todoId);
        return this.todoRepository.deleteItem(todoItem);
    }
}
